#include <iostream>
#include <thread>
#include "singleton.hpp"

using namespace std;

int main()
{   
    cout << "START" << endl;

    this_thread::sleep_for(chrono::milliseconds(1000));

	Singleton::instance().do_something();

	Singleton& singleObject = Singleton::instance();
	singleObject.do_something();
}
