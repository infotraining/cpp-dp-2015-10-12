#include <vector>
#include <memory>

#include "employee.hpp"
#include "hrinfo.hpp"

int main()
{
	using namespace std;

    vector<std::unique_ptr<Employee>> emps;
    emps.emplace_back(new Salary("Jan Kowalski"));
    emps.emplace_back(new Hourly("Adam Nowak"));
    emps.emplace_back(new Temp("Anna Nowakowska"));

	cout << "HR Report:\n---------------\n";
	// generowanie obiektów typu HRInfo
    for(const auto& emp : emps)
	{
        auto hri = emp->create_hrinfo();
		hri->info();
		cout << endl;
	} // wyciek pamięci
}
