#include "shape_factory.hpp"
#include "circle.hpp"

namespace
{
    using namespace Drawing;

    bool is_registered = ShapeFactory::instance().register_creator("Circle", [] { return new Circle();});
}
