#include "shape_factory.hpp"
#include "rectangle.hpp"

//namespace
//{
    using namespace Drawing;

    static bool is_registered = ShapeFactory::instance()
                            .register_creator("Rectangle", [] { return new Rectangle();});
//}

