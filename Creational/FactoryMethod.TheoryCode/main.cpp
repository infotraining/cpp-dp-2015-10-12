#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <list>

#include "factory.hpp"

using namespace std;

class Client
{
    shared_ptr<ServiceCreator> creator_;
public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
	{
        unique_ptr<Service> service = creator_->create_service();

        string result = service->run();
        cout << "Client is using: " << result << endl;
	}
};

using Container = list<int>;

int main()
{
    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
    Factory creators;
    creators.insert(make_pair("CreatorA", make_shared<ConcreteCreatorA>()));
    creators.insert(make_pair("CreatorB", make_shared<ConcreteCreatorB>()));
    creators.insert(make_pair("CreatorC", make_shared<ConcreteCreatorC>()));

    Client client(creators["CreatorC"]);
	client.use();

    Container cont = { 1, 2, 3, 4, 5 };

    for(auto& item : cont)
        cout << item << " ";

//    for(Container::const_iterator it = cont.begin(); it != cont.end(); ++it)
//    {
//        auto& item = *it;
//        cout << item << " ";
//    }
}
