#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <boost/signals2.hpp>

using PriceChangeEvent = boost::signals2::signal<void (std::string, double)>;
using PriceChangeCallback = PriceChangeEvent::slot_type;

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;

    PriceChangeEvent price_changed_;

public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{

	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

	// TODO: rejestracja obserwatora
    boost::signals2::connection attach_observer(const PriceChangeCallback& o)
    {
        return price_changed_.connect(o);
    }

	// TODO: wyrejestrowanie obserwatora

	void set_price(double price)
	{
        if (price != price_)
        {

            price_ = price;
            price_changed_(symbol_, price_);  // publishing an event
        }

		// TODO: powiadomienie inwestorow o zmianie kursu...
	}
};


class Investor //: public Observer
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void update(std::string symbol, double price)
	{
        std::cout << name_ << " notified: " << symbol << " - " << price << std::endl;
	}
};

#endif /*STOCK_HPP_*/
