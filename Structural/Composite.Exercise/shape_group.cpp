#include "shape_group.hpp"

using namespace Drawing;

namespace
{
    bool is_registered =
            ShapeFactory::instance().register_shape("ShapeGroup", new ShapeGroup());
}
