#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <vector>

namespace Drawing
{
    using ShapePtr = std::shared_ptr<Shape>;

    // TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
    class ShapeGroup : public Shape
    {
        std::vector<ShapePtr> shapes_;

    public:
        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            for(const auto& s : source.shapes_)
                shapes_.emplace_back(s->clone());
        }

        ShapeGroup& operator=(const ShapeGroup& source)
        {
            if (this != &source)
            {
                std::vector<ShapePtr> temp_shapes;

                for(const auto& s : source.shapes_)
                    temp_shapes.emplace_back(s->clone());

                shapes_ = std::move(temp_shapes);
            }

            return *this;
        }

        void draw() const
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        void move(int dx, int dy)
        {
            for(const auto& s : shapes_)
                s->move(dx, dy);
        }

        void read(std::istream& in)
        {
            size_t size;
            in >> size;

            for(size_t i = 0; i < size; ++i)
            {
                std::string type_identifier;
                in >> type_identifier;

                auto shp = ShapeFactory::instance().create(type_identifier);
                shp->read(in);

                add_shape(shp);
            }
        }

        void write(std::ostream& out)
        {
            out << "ShapeGroup " << shapes_.size() << std::endl;

            for(const auto& s : shapes_)
                s->write(out);
        }

        Shape* clone() const
        {
            return new ShapeGroup(*this);
        }

        void add_shape(Shape* shape)
        {
            shapes_.emplace_back(shape);
        }
    };

}

#endif /*SHAPE_GROUP_HPP_*/
